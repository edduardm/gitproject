import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientComponent } from './client.component';
import {ClientRoutingModule} from './client.routing';
import {AppSharedModule} from '../shared/app-shared.module';

@NgModule({
  imports: [
    CommonModule, ClientRoutingModule, AppSharedModule
  ],
  declarations: [ClientComponent],
  providers: []
})
export class ClientModule { }
